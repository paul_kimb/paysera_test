/**
 * Created by Paul on 6/10/2017.
 */
import request from 'superagent';

export default function get(requestPath) {
    return new Promise((resolve,reject) => {request
        .get(requestPath)
        .end((err,res)=>{
            if(err){
                reject(res.body);
            }else{
                resolve(res.body);
            }
        });
    })
};