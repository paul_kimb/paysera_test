/**
 * Created by Paul on 6/8/2017.
 */
import get from './apiCalls';
import inputRead from './fileActions';
import { cashInCalc, cashOutNaturalCalc, cashOutJuridicalCalc, setUpForNaturalCashOut } from './calculations';
import ceiling from './ceiling';

/*
* Array of objects to save all configurations
* cfg[0] - Cash_in config
* cfg[1] - Natural user cash_out config
* cfg[2] - Juridical user cash_out config
* cfg[3] - Conversion rates
*/
let cfg = [];

/*
* Main calculation function to check is it cash in, cash out(natural) or cash out(juridical) operation
* and then call specific calculation function for specific operation type.
* @param {Array} inputArray - data from input file
* @resolve {Array} results - returning array of commissions from each operation in same order as operations
*/
const mainCalculations = function mainCalculations(inputArray) {
    return new Promise((resolve) => {
        let results = [];
        let naturalCashOutLimitArray = setUpForNaturalCashOut(inputArray,cfg);
        for (let object of inputArray) {
            if (object.type === 'cash_in') {
                results.push(cashInCalc(object,cfg));
            }
            else if (object.user_type === 'natural') {
                let resultObj = cashOutNaturalCalc(naturalCashOutLimitArray, object,cfg);
                naturalCashOutLimitArray = resultObj.naturalCashOutLimitArray;
                results.push(resultObj.commissions);
            }
            else
                results.push(cashOutJuridicalCalc(object,cfg));
        }
        resolve(results);
    });
};

const printing = function printing(results) {
    for(let value in results){
        console.log(ceiling(results[value]).toFixed(2));
    }
};

/*
* Main code, all "get" functions called as promises one after another to ensure all configurations
* are already in program to start reading input file. "inputRead", "mainCalculations, and "printing"
* functions also called as promises, to ensure that all functions would work synchronously.
 */
get('http://private-38e18c-uzduotis.apiary-mock.com/config/cash-in')
    .then((res) => {
        cfg.push(res);
        get('http://private-38e18c-uzduotis.apiary-mock.com/config/cash-out/natural')
            .then((res) => {
                cfg.push(res);
                get('http://private-38e18c-uzduotis.apiary-mock.com/config/cash-out/juridical')
                    .then((res) => {
                        cfg.push(res);
                        get('http://private-38e18c-uzduotis.apiary-mock.com/rates')
                            .then((res) => {
                                cfg.push(res);
                                inputRead(process.argv[2])
                                    .then((dataFromFile) => {
                                        mainCalculations(dataFromFile)
                                            .then((results) => {
                                                printing(results);
                                            })
                                            .catch((err) => {
                                                console.log('Could not do calculations.')
                                            });
                                    })
                                    .catch((err) => {
                                        console.log('Could not read data. ' + err)
                                    });
                            })
                            .catch((err) => {
                                console.log('Could not set up configuration 4. '+ err)
                            });
                    })
                    .catch((err) => {
                        console.log('Could not set up configuration 3. '+ err)
                    });
            })
            .catch((err) => {
                console.log('Could not set up configuration 2. '+ err)
            });
    })
    .catch((err) => {
        console.log('Could not set up configuration 1'+ err)
    });
