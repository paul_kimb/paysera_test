/**
 * Created by Paul on 6/10/2017.
 */
import moment from 'moment';
import { currencyConversions, currencyCheck, currencyToDefault } from './currencyConversions';

/*
* Function create additional object array to save every user's limit per week
* @param {Array} inputArray - data from file
* @param {Array} cfg - configurations array
* @return {Array} naturalCommissionsLimitsArray - returns array of unique users object with additional information
*   to track their weekly limits
*/
export function  setUpForNaturalCashOut(inputArray,cfg){
    let uniqueUsers = [...new Set(inputArray.map(item => item.user_id))];
    let naturalCommissionsLimitsArray = [];
    uniqueUsers.forEach((user) => {
        naturalCommissionsLimitsArray.push({
            user_id: user,
            year: 0,
            week: 0,
            left: cfg[1].week_limit.amount
        });
    });
    return  naturalCommissionsLimitsArray;
};


/*
* Main cash in calculations
* @param {Object} obj - cash in operation object
* @param {Array} cfg - array of all configurations
* @return {Number} naturalCashOutLimitArray,commissions - updated weekly limits array, calculated commissions
*/
export function cashInCalc(obj,cfg) {
    // if needed currency conversion is done here
    let maxAmount = cfg[0].max.amount;
    let isCurrencySame = currencyCheck(obj.operation.currency,cfg[0].max.currency);
    if(!isCurrencySame)
        maxAmount = currencyConversions(obj.operation.currency, cfg[0].max.currency, cfg[0].max.amount, cfg[3]);
    // after conversion check calculations here
    let commissions = obj.operation.amount * (cfg[0].percents/100);
    if (commissions > maxAmount)
        commissions = maxAmount;
    return commissions;
}

/*
* Main cash out(natural) calculations
* @param {Array} naturalCashOutArray - array with information about
*   users limits and weeks(information from previous operations)
* @param {Object} obj - cash in operation object
* @param {Array} cfg - array of all configurations
* @return {Array,Number} commissions - calculated commissions
*/
export function cashOutNaturalCalc(naturalCashOutArray,obj,cfg) {
    let commissions = 0;
    let limitLeft;
    //searching for user match in weekly limitations array
    naturalCashOutArray.forEach((userLimits, index) => {
        if (obj.user_id === userLimits.user_id) {
            // when user found current limit is assign to variable
            limitLeft = naturalCashOutArray[index].left;
            // currency check
            let isCurrencySame = currencyCheck(obj.operation.currency, cfg[1].week_limit.currency);
            if (!isCurrencySame)
                limitLeft = currencyConversions(obj.operation.currency, cfg[1].week_limit.currency, limitLeft, cfg[3]);
            // checking if the week is still the same from last operation
            if (userLimits.week === moment(obj.date).isoWeek() && userLimits.year === moment(obj.date).year()) {
                //checking if limit is 0
                if (userLimits.left === 0) {
                    commissions = obj.operation.amount * (cfg[1].percents / 100);
                } else {
                    //checking if the current operation amount is bigger than limit left
                    if (obj.operation.amount < limitLeft) {
                        limitLeft = limitLeft - obj.operation.amount;
                        commissions = 0;
                    } else {
                        let amountAfterLimit = obj.operation.amount - limitLeft;
                        limitLeft = 0;
                        commissions = amountAfterLimit * (cfg[1].percents / 100);
                    }
                }
            } else {
                // new week found, in limit array new week is set up
                naturalCashOutArray[index].week = moment(obj.date).isoWeek();
                naturalCashOutArray[index].year = moment(obj.date).year();
                // resetting limit variable amount and adjusting that amount to current operation currency
                if (!isCurrencySame)
                    limitLeft = currencyConversions(obj.operation.currency, cfg[1].week_limit.currency, cfg[1].week_limit.amount, cfg[3]);
                else
                    limitLeft = cfg[1].week_limit.amount;

                //checking if the current operation amount is bigger than limit left
                if (obj.operation.amount < limitLeft) {
                    limitLeft = limitLeft - obj.operation.amount;
                    commissions = 0;
                } else {
                    let amountAfterLimit = obj.operation.amount - limitLeft;
                    limitLeft = 0;
                    commissions = amountAfterLimit * (cfg[1].percents / 100);
                }
            }
            // reverting week limit that is left after operation and saving to the main weekly limitations array
            if (!isCurrencySame)
                naturalCashOutArray[index].left = currencyToDefault(obj.operation.currency, cfg[1].week_limit.currency, limitLeft, cfg[3]);
            else
                naturalCashOutArray[index].left = limitLeft;
        }
    });
    return { naturalCashOutLimitArray: naturalCashOutArray, commissions: commissions };
}

/*
 * Main cash out(juridical calculations
 * @param {Object} obj - cash in operation object
 * @param {Array} cfg - array of all configurations
 * @return {Number} commissions - calculated commissions
 */
export function cashOutJuridicalCalc(obj,cfg) {
    // if needed currency conversion is done here
    let minAmount = cfg[2].min.amount;
    let isCurrencySame = currencyCheck(obj.operation.currency,cfg[2].min.currency);
    if(!isCurrencySame)
        minAmount = currencyConversions(obj.operation.currency, cfg[2].min.currency, cfg[2].min.amount, cfg[3]);
    // after conversion check calculations here
    let commissions = obj.operation.amount * (cfg[2].percents/100);
    if (commissions < minAmount)
        commissions = minAmount;
    return commissions;
}
