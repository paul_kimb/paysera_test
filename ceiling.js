/**
 * Created by Paul on 6/10/2017.
 */
import decimalAdjust from 'decimal-adjust';

/*
* Decimal adjustment of a number.
* https://www.npmjs.com/package/decimal-adjust
*/
Math.ceil10 = function(value, exp) {
    return decimalAdjust('ceil', value, exp);
};

export default function ceiling(value) {
    return parseFloat(Math.ceil10(value,-2));
};