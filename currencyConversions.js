/**
 * Created by Paul on 6/11/2017.
 */
export function currencyConversions(currencyConvert,currencyDefault,value,cfg) {
        const rates = { ...cfg[currencyDefault] };
        for(let rate in rates){
            if(rate === currencyConvert){
                return (value * rates[rate]);
            }
        }

}

export function currencyCheck(currencyObject,currencyCfg) {
    return currencyObject === currencyCfg;
}

export function currencyToDefault(currencyConvert,currencyDefault,value,cfg) {
    const rates = { ...cfg[currencyDefault] };
    for(let rate in rates){
        if(rate === currencyConvert){
            return (value / rates[rate]);
        }
    }
}