/**
 * Created by Paul on 6/10/2017.
 */
import fs from 'fs';

export default function inputRead(file) {
    return new Promise((resolve,reject) => {
        fs.readFile(file, 'utf8', (err, data) => {
            if (err) reject(err);
            resolve(JSON.parse(data));
        });
    });
};