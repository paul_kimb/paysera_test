import { expect } from 'chai';
import { cashInCalc, cashOutNaturalCalc, cashOutJuridicalCalc, setUpForNaturalCashOut } from '../calculations';

//test cfg
const cfg =[{
        "percents": 0.03,
        "max": {
            "amount": 5,
            "currency": "EUR"
        }
    },
    {
        "percents": 0.3,
        "week_limit": {
            "amount": 1000,
            "currency": "EUR"
        }
    },
    {
        "percents": 0.3,
        "min": {
            "amount": 0.5,
            "currency": "EUR"
        }
    },
    {
        "EUR": {
            "USD": 1.1497,
            "JPY": 129.53
        }
    }];

// input.json copy for natural cash out limit array construction
const inputArray = [
    { "date": "2016-01-05", "user_id": 1, "user_type": "natural", "type":
        "cash_in", "operation": { "amount": 200.00, "currency": "EUR" } },
    { "date": "2016-01-06", "user_id": 2, "user_type": "juridical", "type":
        "cash_out", "operation": { "amount": 300.00, "currency": "EUR" } },
    { "date": "2016-01-06", "user_id": 1, "user_type": "natural", "type":
        "cash_out", "operation": { "amount": 30000, "currency": "EUR" } },
    { "date": "2016-01-07", "user_id": 1, "user_type": "natural", "type":
        "cash_out", "operation": { "amount": 1000.00, "currency": "EUR" } },
    { "date": "2016-01-07", "user_id": 1, "user_type": "natural", "type":
        "cash_out", "operation": { "amount": 100.00, "currency": "EUR" } },
    { "date": "2016-01-10", "user_id": 1, "user_type": "natural", "type":
        "cash_out", "operation": { "amount": 100.00, "currency": "EUR" } },
    { "date": "2016-01-10", "user_id": 2, "user_type": "juridical", "type":
        "cash_in", "operation": { "amount": 1000000.00, "currency": "EUR" } },
    { "date": "2016-01-10", "user_id": 3, "user_type": "natural", "type":
        "cash_out", "operation": { "amount": 1000.00, "currency": "EUR" } },
    { "date": "2016-02-15", "user_id": 1, "user_type": "natural", "type":
        "cash_out", "operation": { "amount": 300.00, "currency": "EUR" } }
];

describe('cashInCalc', function() {
    it('calculate cash in commission value', function() {
        let obj1 = { "date": "2016-01-05", "user_id": 1, "user_type": "natural", "type":
            "cash_in", "operation": { "amount": 2000000.00, "currency": "JPY" } };

        let obj2 = { "date": "2016-01-05", "user_id": 1, "user_type": "natural", "type":
            "cash_in", "operation": { "amount": 220.50, "currency": "EUR" } };

        let obj3 = { "date": "2016-01-05", "user_id": 1, "user_type": "natural", "type":
            "cash_in", "operation": { "amount": 20000.00, "currency": "USD" } };
        expect(cashInCalc(obj1,cfg)).to.equal(600);
        expect(cashInCalc(obj2,cfg)).to.equal(0.06615);
        expect(cashInCalc(obj3,cfg)).to.equal(5.7485);
    });
});


describe('cashOutNatural', function() {
    it('calculate cash out(natural) commission value', function() {
        let naturalCashOutLimitArray = setUpForNaturalCashOut(inputArray,cfg);

        let obj1 =  { "date": "2016-01-06", "user_id": 1, "user_type": "natural", "type":
            "cash_out", "operation": { "amount": 2000000.00, "currency": "JPY" } };

        let obj2 = { "date": "2016-01-06", "user_id": 1, "user_type": "natural", "type":
            "cash_out", "operation": { "amount": 220.50, "currency": "EUR" } };

        let obj3 = { "date": "2016-02-07", "user_id": 1, "user_type": "natural", "type":
            "cash_out", "operation": { "amount": 1100, "currency": "EUR" } };

        let res1 = cashOutNaturalCalc(naturalCashOutLimitArray, obj1,cfg);
        let res2 = cashOutNaturalCalc(naturalCashOutLimitArray, obj2,cfg);
        let res3 = cashOutNaturalCalc(naturalCashOutLimitArray, obj3,cfg);
        expect(res1.commissions).to.equal(5611.41);
        expect(res2.commissions).to.equal(0.6615);
        expect(res3.commissions).to.equal(0.3);
    });
});

describe('cashOutJuridical', function() {
    it('calculate cash out(juridical) commission value', function() {
        let obj1 =  { "date": "2016-01-06", "user_id": 1, "user_type": "juridical", "type":
            "cash_out", "operation": { "amount": 2000000.00, "currency": "JPY" } };

        let obj2 = { "date": "2016-01-06", "user_id": 1, "user_type": "juridical", "type":
            "cash_out", "operation": { "amount": 220.50, "currency": "EUR" } };

        let obj3 = { "date": "2016-02-07", "user_id": 1, "user_type": "juridical", "type":
            "cash_out", "operation": { "amount": 175, "currency": "USD" } };

        expect(cashOutJuridicalCalc(obj1,cfg)).to.equal(6000);
        expect(cashOutJuridicalCalc(obj2,cfg)).to.equal(0.6615);
        expect(cashOutJuridicalCalc(obj3,cfg)).to.equal(0.57485);
    });
});