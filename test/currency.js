import { expect } from 'chai';
import { currencyConversions, currencyCheck, currencyToDefault } from '../currencyConversions';

//test cfg
const cfg =[{
        "percents": 0.03,
        "max": {
            "amount": 5,
            "currency": "EUR"
        }
    },
    {
        "percents": 0.3,
        "week_limit": {
            "amount": 1000,
            "currency": "EUR"
        }
    },
    {
        "percents": 0.3,
        "min": {
            "amount": 0.5,
            "currency": "EUR"
        }
    },
    {
        "EUR": {
            "USD": 1.1497,
            "JPY": 129.53
        }
    }];

describe('currencyConversions', function() {
    it('check currency conversion function', function() {
        expect(currencyConversions("USD","EUR",158.58,cfg[3])).to.equal(182.319426);
        expect(currencyConversions("JPY","EUR",168.50,cfg[3])).to.equal(21825.805);
    });
});

describe('currencyConversions', function() {
    it('check currency conversion to default function', function() {
        expect(currencyToDefault("USD","EUR",158.58,cfg[3])).to.equal(137.9316343393929);
        expect(currencyToDefault("JPY","EUR",1649848.50,cfg[3])).to.equal(12737.192156257237);
    });
});
